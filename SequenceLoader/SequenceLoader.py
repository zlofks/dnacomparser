import array
import copy

class SequenceLoader:
    def __init__(self):
        self.sequence = array.array('b', [])
        self.sequence_name = None
        self.file = None
    
    def openFile(self, filePath):
        # open fasta file
        self.file = open(filePath)
    
    def closeSequence(self):
        # complete work with sequence

         self.file.close()
         self.sequence = array.array('b', [])
    
    def getSequence(self):
        return copy.copy(self.sequence)
    
    def readSequence(self):
        # read sequence from fasta file

        self.sequence_name = self.file.readline()

        print("Sequence loading is initiated !")

        for line in self.file.readlines():
            for element in line:
                if element == "A":
                    self.sequence.append(1)
                if element == "G":
                    self.sequence.append(2)
                if element == "T":
                    self.sequence.append(3)
                if element == "C":
                    self.sequence.append(4)
        
        print("Sequence loading is succesful !")
